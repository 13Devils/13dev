import axios from "axios";

const apiClientProduts = axios.create({
  baseURL: "http://localhost:3000",
});

export default {
  namespaced: true,
  state: {
    product: {},
    products: {},
    pagination: {
      itemsPerPage: 6,
      page: 1,
      total: null,
    },
    sort: {
      key: "",
      isAsc: false,
    },
    searchValue: "",
  },
  mutations: {
    stateSingleProduct(state, product) {
      state.product = product[0];
    },
    stateAllProducts(state, products) {
      state.products = products;
    },
    searchValue(state, value) {
      state.pagination = Object.assign({}, state.pagination, { page: 1 });
      state.searchValue = value;
    },
    setPage(state, page) {
      state.pagination = Object.assign({}, state.pagination, { page });
    },
    sort(state, sort) {
      state.sort = sort;
    },
  },
  actions: {
    getProductById(context, id) {
      return apiClientProduts.get("/products?id=" + id).then((response) => {
        context.commit("stateSingleProduct", response.data);
      });
    },
    getAllProducts(context) {
      let params = {};
      if (context.state.pagination.itemsPerPage) {
        params["_limit"] = context.state.pagination.itemsPerPage;
      }
      if (context.state.pagination.page) {
        params["_page"] = context.state.pagination.page;
      }
      if (context.state.searchValue) {
        params["q"] = context.state.searchValue;
      }
      if (context.state.sort.key) {
        params["_sort"] = context.state.sort.key;
      }
      if (context.state.sort.key && context.state.sort.isAsc !== undefined) {
        params["_order"] = context.state.sort.isAsc ? "asc" : "desc";
      }
      return apiClientProduts.get(`/products?`, { params }).then((response) => {
        context.commit("stateAllProducts", response.data);
        context.state.pagination.total = response.headers["x-total-count"];
      });
    },
  },
};
