import Vue from "vue";
import Vuex from "vuex";
import products from "./products";
import users from "./users";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    products,
    users,
  },
});

// https://github.com/typicode/json-server#Plural%20routes тут все апи хуки

// json-server db.json
// json-server dbUser.json --port 4000
