import axios from "axios";

const apiClientProduts = axios.create({
  baseURL: "http://localhost:3000",
});
const apiClientUsers = axios.create({
  baseURL: "http://localhost:4000",
});

export default {
  getProductsPag(config) {
    let params = {};
    if (config.itemsPerPage) {
      params["_limit"] = config.itemsPerPage;
    }
    if (config.page) {
      params["_page"] = config.page;
    }
    if (config.searchStr) {
      params["q"] = config.searchStr;
    }
    if (config.sortValue) {
      params["_sort"] = config.sortValue;
    }
    if (config.sortValue && config.isAsc !== undefined) {
      params["_order"] = config.isAsc ? "asc" : "desc";
    }

    return apiClientProduts.get(`/products?`, { params }).then((response) => {
      return Object.assign({}, response, {
        data: response.data.map((pr) => Object.assign(pr, { show: false })),
      });
    });
  },
  getProductId(id) {
    return apiClientProduts.get("/products?id=" + id);
  },
  getProductUsers(userIds) {
    return Promise.all(
      userIds.map((id) => apiClientUsers.get(`/users?id=${id}`))
    );
  },
};

// https://github.com/typicode/json-server#Plural%20routes тут все апи хуки
// npm install -g json-server

// json-server db.json
// json-server dbUser.json --port 4000
