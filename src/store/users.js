import axios from "axios";

const apiClientUsers = axios.create({
  baseURL: "http://localhost:4000",
});

export default {
  namespaced: true,
  state: {
    users: {},
  },
  getters: {},
  mutations: {
    stateUser(state, users) {
      state.users = users.map((a) => a.data[0]);
    },
  },
  actions: {
    getProductUsers(context, userIds) {
      return Promise.all(
        userIds.map((id) => apiClientUsers.get(`/users?id=${id}`))
      ).then((response) => {
        context.commit("stateUser", response.data);
      });
    },
  },
};
